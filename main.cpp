///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include "main.hpp"
#include "list.hpp"
#include <array>
#include <list>

using namespace std;
using namespace animalfarm;

int Animal::length;
int Animal::randAlphabetIndex; 
char Animal::randomName[10];
char Animal::alphabet[25];
int Animal::i;

int main() {
   srand(time(NULL));

   cout << "Welcome to Animal Farm 4" << endl;
/////////////////////////////// ///////////////////////////////
/////////////////////////////// Animal List ///////////////////////////////
/////////////////////////////// ///////////////////////////////

   SingleLinkedList animalList; // Instantiate a SingleLinkedList

   for( auto i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( (Node*) AnimalFactory::getRandomAnimal() ) ;
   }
   cout << endl;
   cout << "List of Animals" << endl;
   cout << " Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << " Number of elements: " << animalList.size() << endl;
   
   for( auto animal = animalList.get_first() // for() initialize
      ; animal != nullptr // for() test
      ; animal = animalList.get_next( animal )) { // for() increment
      cout << ((Animal*)animal)->speak() << endl;
   }
   
   while( !animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();
      delete animal;
   }
}
