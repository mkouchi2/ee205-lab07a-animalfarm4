#include <iostream>

#include "animal.hpp"

using namespace std;
using namespace animalfarm;

int Animal::length;
int Animal::randAlphabetIndex; 
char Animal::randomName[10];
char Animal::alphabet[25];
int Animal::i;


int main() {
   srand( time(NULL) );

   cout << "Random Gender: [" << Animal::genderName( Animal::getRandomGender() ) << "]" << endl;
   cout << "Random Color: [" << Animal::colorName( Animal::getRandomColor() ) << "]" << endl;
   cout << "Random Bool: [" << boolalpha << Animal::getRandomBool << "]" << endl;
   cout << "Random Weight: [" << Animal::getRandomWeight( 1.2, 5.5 ) << "]" << endl;
   cout << "Random Name: [" << Animal::getRandomName() << "]" << endl;
   
   for ( int i = 0 ; i<25 ; i++ ) {
      Animal* pAnimal = AnimalFactory::getRandomAnimal();
      cout << pAnimal->speak() << endl;
   }
   return 0;
}
