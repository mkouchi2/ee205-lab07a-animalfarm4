///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 3
///
/// @file test2.cpp
/// @version 1.0
///
/// 
///
/// @author Matthew Kouchi <@mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "list.hpp"

using namespace std;
using namespace animalfarm;

void printEmpty(SingleLinkedList list)
{
   if (list.empty())
      cout << "The List is Empty!" << endl;
   else 
      cout << "The List is Not Empty!" << endl;
}

int main() {
   SingleLinkedList list;

   Node* TargetNode1 = new Node;

   //Push new Node to the front of a list
   list.push_front(TargetNode1);  
   
   //Test if list is empty
   printEmpty(list);

   //Remove node from the front of a list
   list.pop_front();
   printEmpty(list);

   //get next and get first
   Node* TargetNode2 = new Node;
   Node* TargetNode3 = new Node;

   list.push_front(TargetNode2); 
   list.push_front(TargetNode3); 
   
   Node* currentNode = list.get_first(); //Returns targetNode3
   cout << list.get_next(currentNode) << endl; //Returns targetNode2

   //Return Number of Nodes
   cout << "The list has " << list.size() << " Elements!" << endl;    
   
   return 0;
}
