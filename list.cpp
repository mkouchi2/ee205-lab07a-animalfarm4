///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.cpp
/// @version 1.0
///
/// Functionality of class SingleLinkedList Members
///
/// @author Matthew Kouchi <@mkouchi2@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "list.hpp"

using namespace std;

namespace animalfarm {

const bool SingleLinkedList::empty() const 
{
   if (head == nullptr) 
      return true;
   else
      return false;
}

void SingleLinkedList::push_front( Node* newNode ) 
{
   newNode->next = head;
   head = newNode;
}

Node* SingleLinkedList::pop_front() 
{
   if (head == nullptr)
      return head;
   else {
      head = head->next;
      return head;
   }
}

Node* SingleLinkedList::get_first() const 
{
   return head;
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const 
{
   return currentNode->next;
}

unsigned int SingleLinkedList::size() const 
{
   int count = 0;
   Node* currentNode = head;
   while (currentNode != NULL) {
      count++;
      currentNode = currentNode->next;   
   }
   return count;
   
}
}
