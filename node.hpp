///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file node.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Matthew Kouchi <@mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

namespace animalfarm {

   class Node {
      protected:
         Node* next = nullptr;
      
      public:
         friend class SingleLinkedList;
      };
}
